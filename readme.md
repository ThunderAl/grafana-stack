Here is simplest self host docker grafana stack.
Copy all `*.example.*` files, modify it as you want and type `docker-compose up -d` to start stack.

You can modify `docker-compose.yml` by adding changes to `docker-compose.<any-name>.yml` and running `docker-compose up -f docker-compose.yml -f docker-compose.<your-name>.yml up -d` or using `./grafana-stack <your-name>`
